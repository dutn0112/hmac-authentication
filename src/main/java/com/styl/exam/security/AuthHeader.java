package com.styl.exam.security;

/**
 * Hold authentication information that is collected from the request.
 */
public class AuthHeader {

    public static final String KEY_API_KEY = "ApiKey";
    public static final String KEY_NONCE = "Nonce";
    public static final String KEY_SIGNATURE = "Signature";

    private final String apiKey;
    private final String nonce;
    private final String signature;

    public AuthHeader(String apiKey, String nonce, String signature) {
        this.apiKey = apiKey;
        this.nonce = nonce;
        this.signature = signature;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getNonce() {
        return nonce;
    }

    public String getSignature() {
        return signature;
    }
}
