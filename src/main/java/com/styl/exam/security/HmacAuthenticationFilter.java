package com.styl.exam.security;

import com.styl.exam.security.hmac.HmacSignature;
import com.styl.exam.security.hmac.HmacUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.MessageDigest;

@Component
public class HmacAuthenticationFilter extends OncePerRequestFilter {

    Logger logger = LoggerFactory.getLogger(HmacAuthenticationFilter.class);

    private final CustomUserDetailsService customUserDetailsService;

    public HmacAuthenticationFilter(CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        AuthHeader authHeader = HmacUtil.getAuthHeader(request);

        // check nonce first
        if (!HmacUtil.isNonceValid(authHeader.getNonce())) {
            logger.error("Nonce is invalid");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Nonce is invalid");
            return;
        }

        UserDetails userDetails = customUserDetailsService.loadUserByApiKey(authHeader.getApiKey());

        // second, check user details
        if (userDetails == null) {
            logger.error("User not found");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "User not found");
            return;
        }

        CachingRequestWrapper requestWrapper = new CachingRequestWrapper(request);

        HmacSignature hmacSignature = new HmacSignature.Builder()
                .requestBody(IOUtils.toString(requestWrapper.getContentAsByteArray()))
                .queryParams(requestWrapper.getQueryString() == null ? "" : requestWrapper.getQueryString())
                .nonce(authHeader.getNonce())
                .build();

        byte[] signature = HmacUtil.createSignature(userDetails.getPassword(), hmacSignature);

        // third, check HMAC signature
        if (!MessageDigest.isEqual(signature, DatatypeConverter.parseBase64Binary(authHeader.getSignature()))) {
            logger.error("Bad signature");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad signature");
            return;
        }

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        filterChain.doFilter(requestWrapper, response);
    }
}
