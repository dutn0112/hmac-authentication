package com.styl.exam.security;

import com.styl.exam.model.User;
import com.styl.exam.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getUserByUsername();

        if (username.equalsIgnoreCase(user.getUsername())) {
            return new CustomUserDetails(user);
        }
        return null;
    }

    public UserDetails loadUserByApiKey(String apiKey) {
        User user = userRepository.getUserByApiKey();

        if (apiKey.equalsIgnoreCase(user.getApiKey())) {
            return new CustomUserDetails(user);
        }
        return null;
    }
}
