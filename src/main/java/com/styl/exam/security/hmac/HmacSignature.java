package com.styl.exam.security.hmac;

/**
 * Hold all ingredients to build a signature
 */
public class HmacSignature {
    private String requestBody;
    private String queryParams;
    private String nonce;

    public HmacSignature(String requestBody, String queryParams, String nonce) {
        this.requestBody = requestBody;
        this.queryParams = queryParams;
        this.nonce = nonce;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(String queryParams) {
        this.queryParams = queryParams;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public static class Builder {
        private String requestBody;
        private String queryParams;
        private String nonce;

        public Builder requestBody(String requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        public Builder queryParams(String queryParams) {
            this.queryParams = queryParams;
            return this;
        }

        public Builder nonce(String nonce) {
            this.nonce = nonce;
            return this;
        }

        public HmacSignature build() {
            return new HmacSignature(requestBody, queryParams, nonce);
        }
    }
}
