package com.styl.exam.security.hmac;

import com.styl.exam.security.AuthHeader;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HmacUtil {

    private static final String DELIMITER = "||";

    /**
     * Collect header params from a request.
     *
     * @param request original request
     * @return AuthHeader
     */
    public static AuthHeader getAuthHeader(HttpServletRequest request) {
        String apiKey = request.getHeader(AuthHeader.KEY_API_KEY);
        String nonce = request.getHeader(AuthHeader.KEY_NONCE);
        String signature = request.getHeader(AuthHeader.KEY_SIGNATURE);

        return new AuthHeader(apiKey, nonce, signature);
    }

    /**
     * Check validness of a nonce from the request.
     * The timestamp which is included in a nonce must be older or newer less than 120 seconds with the current timestamp in the server.
     *
     * @param nonce format [Timestamp]#[RandomString], timestamp is in second.
     * @return true if valid, false otherwise
     */
    public static boolean isNonceValid(String nonce) {
        long requestTime = Long.parseLong(nonce.substring(0, nonce.indexOf("#")));
        long currentTime = System.currentTimeMillis() / 1000;

        return requestTime > currentTime - 120 && requestTime < currentTime + 120;
    }

    /**
     * Create a signature based on the information which has been collected from the request.
     *
     * @param secret
     * @param hmacSignature
     * @return signature in byte array.
     */
    public static byte[] createSignature(String secret, HmacSignature hmacSignature) {
        try {
            final Mac digest = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            digest.init(secretKey);
            if (hmacSignature.getRequestBody() != null && hmacSignature.getRequestBody().length() > 0) {
                digest.update(hmacSignature.getRequestBody().getBytes(StandardCharsets.UTF_8));
                digest.update(DELIMITER.getBytes(StandardCharsets.UTF_8));
            }
            if (hmacSignature.getQueryParams() != null && hmacSignature.getQueryParams().length() > 0) {
                digest.update(hmacSignature.getQueryParams().getBytes(StandardCharsets.UTF_8));
                digest.update(DELIMITER.getBytes(StandardCharsets.UTF_8));
            }
            digest.update(hmacSignature.getNonce().getBytes(StandardCharsets.UTF_8));

            final byte[] signatureBytes = digest.doFinal();
            digest.reset();
            return signatureBytes;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Can't create hmac signature");
        }
    }
}
