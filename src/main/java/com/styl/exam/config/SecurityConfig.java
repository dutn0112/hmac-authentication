package com.styl.exam.config;

import com.styl.exam.security.HmacAuthenticationFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final HmacAuthenticationFilter hmacAuthenticationFilter;

    public SecurityConfig(HmacAuthenticationFilter hmacAuthenticationFilter) {
        this.hmacAuthenticationFilter = hmacAuthenticationFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/**").authenticated();

        http.addFilterBefore(hmacAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
