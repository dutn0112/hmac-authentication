package com.styl.exam.repositories;

import com.styl.exam.model.User;

public interface UserRepository {

    User getUserByUsername();

    User getUserByApiKey();
}
