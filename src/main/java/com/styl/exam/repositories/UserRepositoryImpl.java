package com.styl.exam.repositories;

import com.styl.exam.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Override
    public User getUserByUsername() {
        return new User("admin_user", "admin_user_password", "api-key-07072021");
    }

    @Override
    public User getUserByApiKey() {
        return new User("admin_user", "admin_user_password", "api-key-07072021");
    }
}
