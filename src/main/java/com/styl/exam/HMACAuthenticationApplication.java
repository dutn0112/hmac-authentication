package com.styl.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HMACAuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(HMACAuthenticationApplication.class, args);
    }

}
