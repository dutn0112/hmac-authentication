package com.styl.exam.controllers;

import com.styl.exam.dto.FooDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/foo")
public class FooController {

    private final List<FooDto> data = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<FooDto>> list() {
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<FooDto> add(@RequestBody FooDto dto) {
        data.add(dto);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
