package com.styl.exam;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class ClientApplication {

    public static final String HOST = "http://localhost:8080";
    public static final String KEY_API_KEY = "ApiKey";
    public static final String KEY_NONCE = "Nonce";
    public static final String KEY_SIGNATURE = "Signature";
    public static final String DELIMITER = "||";
    public static final String API_KEY = "api-key-07072021";
    public static final String SECRET_KEY = "admin_user_password";

    public static void main(String[] args) {
        testPostMethod("Hello");
        testPostMethod("World");
        testGetMethod();
    }

    public static void testPostMethod(String content) {
        try {
            String jsonPayload = "{\"text\": \"" + content + "\"}";
            String nonce = System.currentTimeMillis() / 1000 + "#" + randomAlphanumeric();
            URL url = new URL(HOST + "/api/foo");

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.addRequestProperty("Content-Type", "application/json");
            urlConnection.addRequestProperty(KEY_API_KEY, API_KEY);
            urlConnection.addRequestProperty(KEY_NONCE, nonce);
            urlConnection.addRequestProperty(KEY_SIGNATURE, DatatypeConverter.printBase64Binary((createSignature(SECRET_KEY, jsonPayload, "", nonce))));

            try (OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonPayload.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void testGetMethod() {
        try {
            String nonce = System.currentTimeMillis() / 1000 + "#" + randomAlphanumeric();
            URL url = new URL(HOST + "/api/foo");

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.addRequestProperty(KEY_API_KEY, API_KEY);
            urlConnection.addRequestProperty(KEY_NONCE, nonce);
            urlConnection.addRequestProperty(KEY_SIGNATURE, DatatypeConverter.printBase64Binary((createSignature(SECRET_KEY, "", "", nonce))));

            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static String randomAlphanumeric() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private static byte[] createSignature(String secret, String requestBody, String queryParams, String nonce) {
        try {
            final Mac digest = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            digest.init(secretKey);
            if (requestBody != null && requestBody.length() > 0) {
                digest.update(requestBody.getBytes(StandardCharsets.UTF_8));
                digest.update(DELIMITER.getBytes(StandardCharsets.UTF_8));
            }
            if (queryParams != null && queryParams.length() > 0) {
                digest.update(queryParams.getBytes(StandardCharsets.UTF_8));
                digest.update(DELIMITER.getBytes(StandardCharsets.UTF_8));
            }
            digest.update(nonce.getBytes(StandardCharsets.UTF_8));

            final byte[] signatureBytes = digest.doFinal();
            digest.reset();
            return signatureBytes;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Can't create hmac signature");
        }
    }
}
