package com.styl.exam.dto;

public class FooDto {
    private String text;

    public FooDto() {
    }

    public FooDto(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
