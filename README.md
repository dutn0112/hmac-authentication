# HMAC authentication

## Server side
We're using spring boot and spring security for this demonstration.
We have `FooController` which is serving 2 methods.
- http://localhost:8080/api/foo method GET
- http://localhost:8080/api/foo method POST

We also have `HmacAuthenticationFilter` which will be running for every single request to check the authentication of those requests.
Below are the steps on how it works.
1. Extracts the authentication information on the header of each request. (ApiKey, Nonce, Signature)
2. Checks validness of a nonce.
3. From ApiKey, gets the corresponding user.
4. Creates a signature based on the request and the secret key(from step 3).
5. Compares the signature from step 4 and the signature received from the client-side.

## Client side
In the project, we have a class `ClientApplication`, it has two main methods:
- testPostMethod
- testGetMethod

These two methods are going to build a nonce, create a signature, and call to two corresponding methods above on the server-side.

![img.png](images/img.png)

#### Result
![img_1.png](images/img_1.png)
